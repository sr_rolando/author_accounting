class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
      t.string  :title
      t.date    :date
      t.string  :shopname
      t.string  :shopcountry, :default => 'DE'
      t.decimal :price, :precision => 8, :scale => 2
      t.string  :currency, :default => 'EUR'
      t.decimal :exchange_rate, :default => 1
      t.decimal :vat_percent, :default => 19
      t.decimal :vat_amount, :precision => 8, :scale => 2

      t.timestamps
    end
  end
end
